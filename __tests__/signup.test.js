const request = require('supertest');
const app = require('../app'); // Replace 'app' with the path to your Express app
const userService = require('../services/userService')
const bcypt = require('bcrypt')
const bcrypt = require("bcrypt");

jest.mock('../services/userService', () => {
    return {
        createUser: jest.fn(async (username, hashedPasswords) => {
            if(username === 'Existing User') {
                throw new Error('User already exists');
            }
            return {
                message: 'User Created Successfully',
                user: {email: username, password: hashedPasswords},
            };
        }),
        };
});

describe('POST /api/signup', () => {
    it('should create a new user and return a success message', async () => {
        const userData = {
            username: 'John Doe',
            password: 'password',
        };

        const response = await request(app)
            .post('/users/signup')
            .send(userData);
        expect(response.status).toBe(201);
        expect(response.body.message).toBe('User Created Successfully');
    });

    it('should return fail for existing user', async () => {
        const userData = {
            username: 'Existing User',
            password: 'password',
        };

        const response = await request(app)
            .post('/users/signup')
            .send(userData);
        expect(response.status).toBe(409);
        expect(response.body.message).toBe('User already exists');
    });

    it('Check if password is hashed properly', async () => {
        const userData = {
            username: 'John Doe',
            password: 'password',
        };

        const response = await request(app)
            .post('/users/signup')
            .send(userData);
        expect(response.status).toBe(201);
        expect(response.body.message).toBe('User Created Successfully');
        const decryptedPassword = await bcrypt.compare(userData.password, response.body.user.password);
        expect(decryptedPassword).toBe(true);
    });
});
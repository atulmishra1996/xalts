const jwt = require('jsonwebtoken');
const {JWT_TOKEN}  = require('../config');

const requireAuth = async (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ error: 'Authorization token not provided' });
    }

    try {
        const decodedToken =  await jwt.verify(token, JWT_TOKEN);
        req.user = { userId: decodedToken.userId };
        next();
    } catch (err) {
        return res.status(401).json({ error: 'Invalid token' });
    }
};

module.exports = requireAuth;
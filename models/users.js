const {sq} = require("../services/db")
const {DataTypes} = require("sequelize")

const Users = sq.define("users", {
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

   email: {
       type: DataTypes.STRING,
       allowNull: false,
   } ,

   password: {
       type: DataTypes.STRING,
       allowNull: false,
   },

    created_at:{
        type: DataTypes.DATE,
        allowNull: false
    },

    updated_at:{
        type: DataTypes.DATE,
        allowNull: false
    },
});

Users.sync().then(() => {
    console.log("Users table synced");
});

module.exports = Users;
const {sq} = require('../services/db')
const {DataType, DataTypes} = require('sequelize')

const Notifications = sq.define('notifications', {
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

   user_id: {
       type: DataTypes.INTEGER,
       allowNull: false
   },

    message: {
        type: DataTypes.STRING,
        allowNull: false
    },

    status: {
       type: DataTypes.ENUM('open', 'read'),
        allowNull: false
    },

    created_at: {
       type: DataTypes.DATE,
        allowNull: false
    },

    updated_at: {
       type: DataTypes.DATE,
        allowNull: false
    },
});

Notifications.sync().then(() => {
    console.log("Notification table synced");
});

module.exports = Notifications;
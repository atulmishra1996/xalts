const {sq} = require("../services/db")
const {DataTypes} = require('sequelize')

const Processes = sq.define("processes", {
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },

    description: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: "",
    },

    creator_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    status: {
        type: DataTypes.ENUM('created', 'inprogress', 'signedOff'),
        allowNull: false
    },

    visible_user_ids:{
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        allowNull: true,
        defaultValue: [],
    },

    created_at: {
        type: DataTypes.DATE,
        allowNull: false
    },

    updated_at: {
        type: DataTypes.DATE,
        allowNull: false
    }
});

Processes.sync().then(() => {
    console.log("Process table synced");
});

module.exports = Processes;

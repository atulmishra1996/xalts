const {sq} = require('../services/db')
const {DataTypes} = require('sequelize')

const ProcessMeta = sq.define('process_meta', {
    id:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    process_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
    },

    comment:{
        type: DataTypes.STRING,
        allowNull: true
    },

    image_path:{
        type: DataTypes.STRING,
        allowNull: false
    },

    user_id:{
        type: DataTypes.INTEGER,
        allowNull: false,
    },

    status:{
        type: DataTypes.ENUM('pending', 'signedOff'),
        allowNull: false
    },

    created_at:{
        type: DataTypes.DATE,
        allowNull: false
    },

    updated_at:{
        type: DataTypes.DATE,
        allowNull: false
    },
});

ProcessMeta.sync().then(() => {
    console.log("Process Meta table synced");
});

module.exports = ProcessMeta;
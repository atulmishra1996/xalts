var express = require("express");
var router = express.Router();
const friendsController = require('../controllers/friendsController')
const requireAuth = require('../middleware/auth')

router.get('/', requireAuth, friendsController.getFriends);

module.exports = router;
var express = require("express");
var router = express.Router();
const notificationController = require('../controllers/notificationsController')
const requireAuth = require('../middleware/auth')

router.get('/', requireAuth, notificationController.getAllNotifications);

module.exports = router;
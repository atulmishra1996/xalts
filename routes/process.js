var express = require("express");
var router = express.Router();
const processController = require('../controllers/processController');
const requireAuth = require("../middleware/auth")

router.post('/', requireAuth, processController.createProcess);
router.put('/:process_id/comment', requireAuth, processController.createComment);
router.get('/:process_id', requireAuth, processController.getAllComments);

module.exports = router;
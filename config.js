const env = process.env;
const config = {
    db: { /* do not put password or any sensitive info here, done only for demo */
        host: env.DB_HOST || '127.0.0.1',
        port: env.DB_PORT || '5432',
        user: env.DB_USER || 'atulmishra',
        password: env.DB_PASSWORD || 'atulmishra',
        database: env.DB_NAME || 'xalts',
    },
    listPerPage: env.LIST_PER_PAGE || 10,
};

const JWT_TOKEN = 'dajndakjbdyabdkhsabkjdsad';

// module.exports = config;
module.exports = { config, JWT_TOKEN };
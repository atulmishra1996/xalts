var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');


var usersRouter = require('./routes/users');
var processRouter = require('./routes/process');
var friendsRouter = require('./routes/friends');
var notificationRouter = require('./routes/notifications');
var requireAuth = require('./middleware/auth');

var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.use('/users', usersRouter);
app.use(requireAuth);
app.use('/api/process', processRouter);
app.use('/api/friends', friendsRouter);
app.use('/api/notifications', notificationRouter);

module.exports = app;

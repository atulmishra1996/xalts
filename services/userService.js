const Users = require("../models/users");
const createUser = async (username, hashedPasswords) => {
    const isExistingUser = await Users.findOne({
        where: {email: username}
    })

    if(!isExistingUser) {
        const newUser = await Users.create({
            email: username,
            password: hashedPasswords,
        });
        return {message: "User Created Successfully", user: newUser.toJSON()};
    } else {
        throw new Error("User Already Exist");
    }
}

module.exports = {createUser};
const { Pool } = require('pg');
const config = require('../config');
const pool = new Pool(config.db);
const { Sequelize } = require("sequelize");

const sequelize = new Sequelize(config.config.db.database, config.config.db.user, config.config.db.password, {
   host: config.config.db.host,
   dialect: 'postgres',
    define: {
       timestamps: true,
        updatedAt: 'updated_at',
        createdAt: 'created_at',
    },
});

sequelize
    .authenticate()
    .then(() => console.log('Database connection has been established successfully.'))
    .catch((err) => console.error('Unable to connect to the database:', err));

module.exports = {
    sq: sequelize
};
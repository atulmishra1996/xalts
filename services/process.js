const ProcessMeta = require('../models/processMeta')
const Process = require('../models/processes')
const Notifications = require('../models/notifications')

const checkAndUpdateProcessStatus = async (processId) => {
    try {
        console.log("Checking for process update  -- "+ processId)
        const comments = ProcessMeta.findAll({
            where: { process_id: processId }
        });

        console.log("Successfully fetch all comments" + (await comments).length)

        if(comments){
            const allCommentsDone = (await comments).every((comment) => comment.status === 'signedOff');

            if(allCommentsDone){
                const process = await Process.findOne({
                    where: {id: processId}
                });

                if(process) {
                    process.status = 'signedOff';

                    await process.save();
                    await Notifications.create({
                        user_id: process.creator_id,
                        message: "Process Sign Off Completed",
                        status: 'open',
                    })
                } else {
                    console.log("Something went wrong while saving process status");
                }
            } else {
                console.log("All comments are still not done")
            }
        } else {
            console.log("Something went wrong while fetching comments");
        }
    } catch (err){
        console.log("Something went wrong" + err);
    }
}


module.exports = checkAndUpdateProcessStatus
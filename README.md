# XAlts

Short description or introduction to your Node.js project.

## Table of Contents

- [Project Name](#project-name)
    - [Table of Contents](#table-of-contents)
    - [Introduction](#introduction)
    - [Features](#features)
    - [Requirements](#requirements)
    - [Getting Started](#getting-started)
        - [Installation](#installation)
        - [Configuration](#configuration)
    - [Usage](#usage)
    - [API Documentation](#api-documentation)
    - [Testing](#testing)
    - [Contributing](#contributing)
    - [License](#license)
    - [Acknowledgments](#acknowledgments)

## Introduction

1. Postgres Database and Login:

○ Create a PostgresSQL database to store user information such as name
and email.

○ Develop a login system that allows users to sign up and assigns them an
internal login ID.

○ Implement a bonus feature to allow users to connect their Metamask
wallet.

2. Multisignature Process:

○ Develop a multi-signature process where a user can create a process that
requires sign-offs from five other users.

○ Allow the user to choose the five other users from a dropdown list and
send email notifications to each user when a new process is created.

○ Add the functionality for users to add comments and upload a mandatory
picture during the sign-off process.

○ Allow the process creator to select which users can see the comments.

○ Ensure the process creator receives a notification on their page when
anyone signs off, and notify all parties involved via email when everyone
signs off.
 

## Features

1 - User can signup and perform login.

2 - JWT Token is issued for user after login operations.

3 - All remaining endpoints need these JWT Token for authentication.

4 - User can create process and tag other users for there sign-off's and can also choose which users will have visibility over comments.

5 - Tagged Users can comment and provide sign-off's.

6 - Once all tagged user's provide sign-off, process will be marked as completed(signed-off).

7 - Have also implemented Notifications :

    i -  All Tagged users will get notification in the process they are tagged
    ii - After All tagged users have signed-off, process creator will get notification for process have been completed.
## Requirements

NPM version - 9.6.7

## Getting Started


### Installation

Please use package.json to install dependencies

### Configuration

Update database configurations in services/db.js to match your local PSQL needs.

## Usage

run npm start

## API Documentation

https://api.postman.com/collections/820048-a44ea8e9-8cc4-471e-8f70-9f15d8eea9da?access_key=PMAT-01H6NC4XT3T5EHQ8F9SACA6V2K

## Project Structure

Project Structure:
- /XAlts
    - /controllers  # Controllers to handle API endpoints
    - /models       # Database models and business logic
    - /routes       # Express routes
    - /services     # Database config and services
    - /middlewares  # Authenticating JWT Token
- /XAlts
    - config.js   # PostgreSQL database configuration
    - /services/db.js #Intilizing Sequelize for PSQL
- app.js        # Main entry point for the backend application

Database Tables:
- users
    - id (Primary Key)
    - user_name
    - password

- processes
    - id (Primary Key)
    - creator_id (Foreign Key to users table)
    - status (e.g., 'created', 'inprogress', 'signedOff')
    - visible_to_user_ids (JSON array of user_ids who can see comments)
    - name
    - description
    - created_at
    - updated_at

- process_meta
    - id (Primary Key)
    - user_id (Foreign Key to users table)
    - process_id (Foreign Key to multi_signature_process table)
    - comment
    - image_path (optional)
    - created_at
    - updated_at
    - status(e.g. 'pending', 'signedOff')

- Notification
    - id (Primary Key)
    - user_id (Foreign Key to users table)
    - message (Foreign Key to multi_signature_process table)
    - created_at
    - updated_at
    - status(e.g. 'open', 'read')


API Endpoints:
- User Registration:
    - POST /users/signup
    - Request Body: { username, password }
    - Response: { message: "User Created successfully" }

- User Login:
    - POST /users/login
    - Request Body: { email, password }
    - Response: { message: "Login successful", token }

- Create Multi-Signature Process:
    - POST /api/process
    - Request Body: { name, description, signOffUsers: [userIds], visibleToUsers: [userIds] }
    - Response: { message: "Process Created Successfully", processId }

- Sign Off on Multi-Signature Process ( Comment):
    - POST /api/api/process/:process_id/comment
    - Request Body: {comment, picture (optional) }
    - Response: { message: "Sign-off successful" }

- Get Multi-Signature Process Details:
    - GET /api/process/:processId
    - Response: { processDetails }

- Get User Notifications:
    - GET /api/notifications/:userId
    - Response: { notifications }


- Authentication and Authorization:
    - Used JSON Web Tokens (JWT) for authentication and authorize access to specific endpoints based on user roles.

## Improvement:
 
- Event Driven System can be introduced for notifications for better handling.
- For Authorization - Roles and Permission table can be added and while performing login, this role can be added with JWT Token for middleware authentications.
- Test cases can be added to improve code coverage and handling corner cases.

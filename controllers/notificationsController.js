var express = require("express");
var router = express.Router();
const Notifications = require("../models/notifications");


const getAllNotifications = async (req, res) =>{
    try {
        const userId = req.user.userId

        const notifications = await  Notifications.findAll({
            where: {
                user_id: userId,
                status: 'open',
                }
        });

        if(notifications){

            for (const notification of notifications) {
                notification.status = 'read';
                await notification.save();
            }

            res.status(200).json({notifications});
        }
    } catch (err) {
        console.error('Error retriving notification: ', err);
        res.status(500).json({error: 'Internal Server error'});
    }
}

module.exports = { getAllNotifications }
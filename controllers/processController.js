const Processes = require('../models/processes')
const ProcessMeta = require('../models/processMeta')
const Notifications = require('../models/notifications')
const {Op} = require('sequelize')
const checkAndUpdateProcessStatus = require('../services/process')

const createProcess = async (req, res) => {
    try {
        const {name, description, visible_user_ids, authorized_users_ids } = req.body

        console.log(name + " : " + description + " : " + visible_user_ids + " : " + authorized_users_ids + " :: " + req.user.userId)

        const process = await Processes.create({
            name: name,
            description: description,
            creator_id:  req.user.userId,
            visible_user_ids: visible_user_ids,
            status: 'created',
        });

        for (const signOffUserId of authorized_users_ids) {
            const processMeta = await ProcessMeta.create({
                process_id: process.id,
                comment: "",
                image_path: "",
                user_id: signOffUserId,
                status: 'pending',
            });

            const notification = await Notifications.create({
                user_id: signOffUserId,
                message: "Pending Sign Off",
                status: 'open',
            })
        }

        res.status(201).json({message: "Process Created Successfully", user: process.toJSON()});
    } catch (err) {
        console.error("Error creating Process:", err);
        res.status(500).json({message: "Error creating Process", err});
    }
}

const createComment = async (req, res) => {
    try {
        const process_id = req.params.process_id
        const {comment, image_path} = req.body

        console.log("User id and process id " + req.user.userId + " -- " + process_id)
        const commentData = await ProcessMeta.findOne({
            where: {
                process_id: process_id,
                user_id: req.user.userId
            }
        });

        const commentDataJSON = commentData.toJSON();
        console.log("Comment Data -->" + commentDataJSON)
        if(commentData) {
            commentData.comment = comment
            commentData.image_path = image_path
            commentData.status = 'signedOff'

            await commentData.save();
            await checkAndUpdateProcessStatus(process_id);

        }
        res.status(201).json({message: "Process Meta Created Successfully", user: commentData.toJSON()});
    } catch (err) {
        console.error("Error creating Process Meta:", err);
        res.status(500).json({message: "Error creating Process Meta", err});
    }
}

const getAllComments = async (req, res) => {
    try{
        const processId = req.params.process_id
        const userId =  req.user.userId

        console.log("Checking for :: " + processId + "-- for user -> " + userId)

        const process = await Processes.findOne({
            where: { id: processId,
                visible_user_ids: {[Op.contains]: [userId]}
            },
        });

        if (process){
            const comments = await ProcessMeta.findAll({
                where: {process_id: processId,  status: 'waiting'}
            });

            if(comments){
                res.status(200).json({comment: {comments}});
            } else {
                console.log("Something went wrong while fetching all comments");
                res.status(404).json({message: "Something went wrong while fetching all comments"});
            }
        } else {
            console.log("User Not Authorized to see comments");
            res.status(200).json({message: "No Comments Found"});
        }

     } catch (err) {
        console.error("Error Fetching Comment:", err);
        res.status(500).json({message: "Error Fetching Comment", err: err});
    }
}


module.exports = {createProcess, createComment, getAllComments};
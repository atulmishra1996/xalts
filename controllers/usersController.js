const bcrypt = require("bcrypt");
const Users = require("../models/users");
const jwt = require('jsonwebtoken');
const { config, JWT_TOKEN} = require('../config')
const {createUser} = require('../services/userService')

const signUp = async (req, res) => {
    try {
        console.log("came for sign up req")
        const {username, password} = req.body
        const saltRounds = 10;

        const salt = await bcrypt.genSalt(saltRounds);

        const hashedPasswords = await bcrypt.hash(password, salt);
        const resultJson = await createUser(username, hashedPasswords)
        return res.status(201).json(resultJson);

    } catch (err) {
        console.error("Error creating user:", err.message);
        return res.status(409).json({message: err.message});
    }
};

const login = async (req, res) => {
    try {
        const {username, password} = req.body

        const user = await Users.findOne({where: { email: username } });
        if (!user) {
            // User not found
            return res.status(404).json({ error: 'User not found' });
        }

        const isPasswordMatch = await bcrypt.compare(password, user.password);

        if (isPasswordMatch) {
            const token = jwt.sign({ userId: user.id }, JWT_TOKEN, { expiresIn: '12h' });

            return res.status(200).json({ message: 'Login successful', token: token});
        } else {
            return res.status(401).json({ error: 'Invalid credentials' });
        }
    } catch (err) {
        console.error("Error Fetching user:", err);
        res.status(500).json({message: "Error Fetching user", err});
    }
}

module.exports = {signUp , login};
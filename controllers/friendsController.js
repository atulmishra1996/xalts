var express = require("express");
var router = express.Router();
const Users = require("../models/users");
const {Op} = require('sequelize')

const getFriends = async (req, res) =>{
    try {
        const userId = req.user.userId

        const usersFriends = await  Users.findAll({ where: { id: {[Op.ne]: userId}}});

        res.status(200).json({usersFriends});
    } catch (err) {
        console.error('Error retriving users: ', err);
        res.status(500).json({error: 'Internal Servor error'});
    }
}

module.exports = { getFriends }